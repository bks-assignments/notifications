﻿namespace Notifications
{
    public enum EntityType
    {
        Course,
        Resource,
        Event
    }
}