﻿using System;

namespace Notifications
{
    [Flags]
    public enum TargetAudience
    {
        User,
        Manager,
        SeniorManager
    }
}