# Assigment: User notifications

**Objective:**
Refactor the legacy project towards robust and maintainable code. 

**Constraints:**
You are free to change everything but keep the functionality.
That means that the system must support existing types of notifications and their restrictions. But the way they are handled could be changed.

**Hints:**
Consider adding a new notification type. Or creating another communication channel (API controller, mobile gateway, event handler, etc).
Would you copy the same logic or there is a way to encapsulate domain-specific logic? To ensure that all the restrictions will be validated regardless of a channel?
